using ApplicationCore.Interfaces;
using Model.Scheduler;

namespace Infrastructure.psql
{
    using Microsoft.EntityFrameworkCore;

#pragma warning disable CS1591
    public class NpgsqlContext : DbContext, IContextRepository
    {
        public DbSet<PushMessage> PushMessages { get; set; }

        public NpgsqlContext(DbContextOptions<NpgsqlContext> options)
            : base(options)
        { }
        public bool SaveSchedulerMessage(SchedulerMessage message) {
            return false;
        }
    }
#pragma warning restore CS1591
}