
namespace Model.Scheduler
{
    public class SchedulerMessage
    {
        public string PublisherId { get; set; }
        public string Username { get; set; }
        public string AppId { get; set; }
        public string Platform { get; set; }
        public string Topics { get; set; }
        public int QuickDeliveryTimeout { get; set; }
        public bool SendSynchronously { get; set; }
        public string FcmLegacyServerKey { get; set; }
        public bool SendLegacyGcm { get; set; }
        public string UniqueAppId { get; set; }
        public Data Data { get; set; }
        public string Status { get; set; }
        public string ErrorReason { get; set; }
        public string ErrorMessage { get; set; }

        public bool IsValidMessage()
        {
            return true;
        }
    }
    
    public class Data
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public string LocalImg { get; set; }
        public string ExternalImg { get; set; }
        public string Action { get; set; }
        public string ExternalAction { get; set; }
        public string ButtonText { get; set; }
        public string Alert { get; set; }
        public string Badge { get; set; }
        public string Sound { get; set; }
        public string PushWebServer { get; set; }
    }
}