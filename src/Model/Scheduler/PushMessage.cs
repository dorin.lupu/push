namespace Model.Scheduler
{
    public class PushMessage
    {
        public int Id { get; set; }
        public string PublisherId { get; set; }
        public string AppOwnerUsername { get; set; }
        public string AppId { get; set; }
        public string PwaPublicKey { set; get; }
        public string PwaPrivateKey { set; get; }
    }
}