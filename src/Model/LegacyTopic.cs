using Microsoft.AspNetCore.Mvc;

namespace Model
{
    public class LegacyTopic
    {
        [BindProperty(Name = "publisherId")] public string PublisherId { get; set; }
        [BindProperty(Name = "username")] public string AppOwnerUsername { get; set; }
        [BindProperty(Name = "appId")] public string AppId { get; set; }
        [BindProperty(Name = "deviceId")] public string DeviceId { get; set; }
        [BindProperty(Name = "type")] public string Type { get; set; }

        public string Topics { get; set; }

        public LegacyTopic()
        {
            PublisherId = "";
        }
    }
}