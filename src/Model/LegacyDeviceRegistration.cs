﻿using Microsoft.AspNetCore.Mvc;

namespace Model
{
    public class LegacyDeviceRegistration
    {
        [BindProperty(Name = "publisherId")] public string PublisherId { get; set; }
        [BindProperty(Name = "username")] public string AppOwnerUsername { get; set; }
        [BindProperty(Name = "appId")] public string AppId { get; set; }
        [BindProperty(Name = "deviceId")] public string DeviceId { get; set; }
        [BindProperty(Name = "registrationId")] public string RegistrationId { get; set; }

        public LegacyDeviceRegistration()
        {
            PublisherId = "";
        }
    }
}