﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using Microsoft.Extensions.Logging;
using Model;

namespace ApplicationCore.GCM
{
    public class GcmService : ICloudMessageService
    {
        public const int MAX_BIG_MESSAGE_WORKERS = 12;
        private const int MaxMessagesInQueue = 1000; 
        public IList<IMessageWorker> Workers { get; }
        public ConcurrentQueue<Message> Queue { get; private set; } 
        private ConcurrentQueue<DeviceRegistration> RegistrationsQueue { get; set; }

        private IMaintenanceWorker MaintenanceWorker { get; }

        private readonly IRepository _repository;
        private readonly ILogger<GcmService> _logger;

        private volatile int _deviceRegistrationsProcessed;
        private volatile int _deviceRegistrationsDeleted;

        public int DeviceRegistrationsProcessed
        {
            get => _deviceRegistrationsProcessed;
            set => _deviceRegistrationsProcessed = value;
        }

        public int DeviceRegistrationsDeleted
        {
            get => _deviceRegistrationsDeleted;
            set => _deviceRegistrationsDeleted = value;
        }

        public GcmService(IRepository repository, ILogger<GcmService> logger, IList<IMessageWorker> workers,
            IMaintenanceWorker maintenanceWorker)
        {
            _repository = repository;
            _logger = logger;
            Workers = workers;
            MaintenanceWorker = maintenanceWorker;
        }

        public async Task Start()
        {
            Queue = new ConcurrentQueue<Message>();
            RegistrationsQueue = new ConcurrentQueue<DeviceRegistration>();

            for (var i = 1; i < Workers.Count; i++)
            {
                var worker = Workers[i];
                worker.Start(this);
                await Task.Delay(41 * i);
                _logger.LogWarning("GCM Worker " + i + "-" + worker.Id + " started");
            }

            _logger.LogWarning("GCM Maintenance Worker created: " + MaintenanceWorker.Id);
            MaintenanceWorker.Start(this);
        }

        public void Stop()
        {
            var tasksToWait = new Task[Workers.Count];
            for (var i = 0; i < Workers.Count; i++)
            {
                Workers[i].Stop();
                tasksToWait[i] = Workers[i].Task;
            }

            MaintenanceWorker.Stop();

            try
            {
                Task.WaitAll(tasksToWait);
            }
            catch (AggregateException e)
            {
                foreach (var v in e.InnerExceptions)
                {
                    _logger.LogError("Task.WaitAll exception msg: " + v.Message);
                }
            }

            _logger.LogWarning("All GCM workers stopped");

            try
            {
                Task.WaitAll(new Task[] { MaintenanceWorker.Task });
            }
            catch (AggregateException e)
            {
                foreach (var v in e.InnerExceptions)
                {
                    _logger.LogError("Task.WaitAll exception msg: " + v.Message);
                }
            }

            _logger.LogInformation("APN Maintenance worker " + MaintenanceWorker.Id + " stopped");
        }

        public bool Enqueue(Message message)
        {
            if (Queue.Count >= MaxMessagesInQueue) return false;
            Queue.Enqueue(message);
            return true;
        }

        public void RegisterDevice(DeviceRegistration registration)
        {
            RegistrationsQueue.Enqueue(registration);
        }

        private async Task PersistCachedRegistrations()
        {
            var success = 0;
            var fail = 0;

            if (RegistrationsQueue.Count == 0)
            {
                return;
            }

            _logger.LogInformation("Started persisting GCM registrations: " + RegistrationsQueue.Count);

            try
            {
                var result = await _repository.GCMRegister(RegistrationsQueue);

                success = result["success"];
                fail = result["fail"];
                DeviceRegistrationsProcessed += result["deviceRegistrationsProcessed"];
            }
            catch (Exception ex)
            {
                _logger.LogError("Error persisting GCM registrations: " + ex);
            }

            _logger.LogInformation("Persisted GCM registrations: success=" + success + ", fail=" + fail + ", total=" +
                                   DeviceRegistrationsProcessed);
        }

        public async Task DoMaintenance()
        {
            await PersistCachedRegistrations();
        }
        
        public string GetStats()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("GCM Maintenance Worker: ").Append("ID=").Append(MaintenanceWorker.Id).Append(", ")
                .Append("status=").Append(MaintenanceWorker.Status).Append("\n\n");

            sb.Append("GCM Workers (").Append(Workers.Count).Append("):").Append("\n");

            foreach (GcmMessageWorker worker in Workers)
            {
                sb.Append("ID=").Append(worker.Id).Append(", ")
                    .Append("status=").Append(worker.Status).Append(", ")
                    .Append("currentMessage=")
                    .Append(worker.CurrentMessage != null ? worker.CurrentMessage.ToString() : "(null)")
                    .Append("\n\n");
            }

            sb.Append("\n");

            Message[] queuedMessages = Queue.ToArray();
            sb.Append("GCM Queue (").Append(queuedMessages.Length).Append("):").Append("\n");
            foreach (Message message in queuedMessages)
            {
                sb.Append("ID=").Append(message.Id).Append(", ")
                    .Append("status=").Append(message.Status).Append(", ")
                    .Append("appId=").Append(message.PublisherId).Append(".").Append(message.AppOwnerUsername)
                    .Append(".").Append(message.AppId)
                    .Append("data=").Append(message.GetDataString())
                    .Append("\n\n");
            }

            sb.Append("\n");

            DeviceRegistration[] registrations = RegistrationsQueue.ToArray();
            sb.Append("GCM Registrations Queue (").Append(registrations.Length).Append("):\n");
            int count = 0;
            foreach (DeviceRegistration registration in registrations)
            {
                sb.Append(registration.ToString()).Append("\n\n");

                if (++count >= 10)
                {
                    sb.Append("And " + (registrations.Length - count) + " more registrations\n\n");
                    break;
                }
            }

            sb.Append("\n");

            sb.Append("GCM Registrations processed: ").Append(DeviceRegistrationsProcessed);

            sb.Append("\n\n");
            
            return sb.ToString();
        }
    }
}