﻿using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using ApplicationCore.APNS;
using ApplicationCore.Interfaces;
using Microsoft.Extensions.Logging;
using Model;


namespace ApplicationCore.GCM
{
    public class GcmMessageWorker : IMessageWorker

    {
    private const int GCM_MULTICAST_SIZE = 1000; // MUST be <= 1000 (GCM restriction)
    private const int MAX_ERROR_STRING_LENGTH = 5000;

    public string Id { get; set; }
    public Task Task { get; set; }

    private volatile string _status;

    public string Status
    {
        get => _status;
        private set => _status = value;
    }

    private volatile Message _currentMessage;

    public Message CurrentMessage
    {
        get => _currentMessage;
        private set => _currentMessage = value;
    }

    private CancellationTokenSource CancellationTokenSource { get; }
    private GcmService Service { get; set; }

    private readonly IRepository _repository;
    private readonly SCEnvironment _scEnvironment;
    private readonly ILogger<GcmMessageWorker> _logger;


    public GcmMessageWorker(IRepository repository, SCEnvironment scEnvironment,
        ILogger<GcmMessageWorker> logger, string id)
    {
        _repository = repository;
        _scEnvironment = scEnvironment;
        _logger = logger;
        Id = id;
        CancellationTokenSource = new CancellationTokenSource();
    }

    private class MessageSendException : Exception
    {
        public MessageSendException(string message)
            : base(message)
        {
        }
    };

    private class MessageSendServerKeyException : Exception
    {
        public MessageSendServerKeyException(string message)
            : base(message)
        {
        }
    }

    public void Start(GcmService service)
    {
        Service = service;
        Task = Task.Factory.StartNew(async () => await DoWork(), TaskCreationOptions.LongRunning);
        Task.ContinueWith((t) => { _logger.LogCritical("Failed task for worker" + Id + ":\n" + t.Exception); },
            TaskContinuationOptions.OnlyOnFaulted);

        Status = IMessageWorker.StatusStarted;
    }

    public void Start(ApnService apnService)
    {
        throw new NotImplementedException();
    }

    public void Stop()
    {
        CancellationTokenSource.Cancel();
    }

    private async Task ProcessMessage(Message message)
    {
        try
        {
            if (message.Registrations == null && !message.IsTestMessage)
            {
                if (string.IsNullOrEmpty(message.FcmLegacyServerKey))
                {
                    message.Status = Message.STATUS_FAILED;
                    message.ErrorReason = Message.ERROR_FCM_CONFIGURATION;
                    message.ErrorMessage = "The fcm key is null or empty";
                    _logger.LogError("Worker " + Id + " too many big messages processing. Failing message: " +
                                     message);
                }
                else
                {
                    message.Registrations = await _repository.GetGCMRegistrations(message);
                    message.ServerEndPoint = "FCM";
                }

                _logger.LogInformation("Worker " + Id + " got registrations for message: " + message);
            }
            else if (message.IsTestMessage && message.RegistrationsTotal == -1)
            {
                Thread.Sleep(10000); // simulate slow DB read
                message.RegistrationsTotal = 60000;
            }

            if (message.IsBig() && GetBigMessageWorkersCount() > GcmService.MAX_BIG_MESSAGE_WORKERS)
            {
                message.Status = Message.STATUS_FAILED;
                message.ErrorReason = Message.ERROR_OVER_CAPACITY;
                message.ErrorMessage = "Server is overloaded. Try again later.";
                _logger.LogError( "Worker Id: {0}  too many big messages processing. Failing message: {1}", Id, message );
            }
            else
            {
                await SendMessages(message, message.Registrations, message.FcmLegacyServerKey);
                message.Status = Message.STATUS_DELIVERED;
                _logger.LogInformation("Worker " + Id + " delivered message: " + message);
            }
        }
        catch (MessageSendException e)
        {
            message.Status = Message.STATUS_FAILED;
            message.ErrorReason = Message.ERROR_INTERNAL_SERVER_ERROR;
            if (e.Message == GcmMessageSender.ERROR_MESSAGE_TOO_BIG)
            {
                message.ErrorReason = Message.ERROR_PAYLOAD_TOO_BIG;
            }

            // the message.ErrorMessage already has error details, so no need adding it here
            _logger.LogError("Worker " + Id + " error sending message: " + message + ", error:\n" + e);
        }
        catch (MessageSendServerKeyException e)
        {
            message.Status = Message.STATUS_FAILED;
            message.ErrorReason = Message.ERROR_FCM_CONFIGURATION;
            _logger.LogError("Worker " + Id + " error sending message: " + message + ", error:\n" + e);
        }
        catch (Exception e)
        {
            message.Status = Message.STATUS_FAILED;
            message.ErrorReason = Message.ERROR_INTERNAL_SERVER_ERROR;
            message.ErrorMessage += e.Message;
            _logger.LogError("Worker " + Id + " error sending message: " + message + ", error:\n" + e);
        }
    }

    private async Task DoWork()
    {
        Status = IMessageWorker.StatusWaiting;

        // TODO: process messages that are still in queue when cancellation is requested
        while (!CancellationTokenSource.IsCancellationRequested)
        {
            if (!Service.Queue.TryDequeue(out var message))
            {
                await Task.Delay(100);
                continue;
            }

            CurrentMessage = message;
            Status = IMessageWorker.StatusRunning;
            await ProcessMessage(message);
            Status = IMessageWorker.StatusWaiting;

            _logger.LogInformation("Worker " + Id + " cancellation requested");
        }
    }

    /// <summary>
    /// This is not 100% accurate considering multiple threads 
    /// but it is good enough for our purpose
    /// </summary>
    private int GetBigMessageWorkersCount()
    {
        return Service.Workers.Cast<GcmMessageWorker>().Where(worker => worker != null).Count(worker =>
            worker.Status == IMessageWorker.StatusRunning && worker.CurrentMessage.IsBig());
    }

    private async Task SendMessages(Message message, List<string> registrations, string fcmLegacyServerKey)
    { 
        var invalidRegistrationsQueue = new List<string>();
        var sender = new GcmMessageSender(_repository, _logger);
        var errorStringBuilder = new StringBuilder(MAX_ERROR_STRING_LENGTH + 1000);
        var truncated = false;
        for (var i = 0; i < registrations.Count; i += GCM_MULTICAST_SIZE)
        {
            // take next GCM_MULTICAST_SIZE registrations or what's left in the list
            var rangeCount = Math.Min(GCM_MULTICAST_SIZE, registrations.Count - i);
            var partialRegistrations = registrations.GetRange(i, rangeCount);

            var result = await sender.Send(message.Data, partialRegistrations, fcmLegacyServerKey);

            message.RegistrationsProcessed += partialRegistrations.Count;
            message.RegistrationsDelivered += result.success;
            message.RegistrationsUpdated += result.canonicalIds;
            message.RegistrationsFailed += result.failure;
            message.RegistrationsUnregistered += result.unregister;

            invalidRegistrationsQueue.AddRange(result.invalidRegistrations);

            if (!string.IsNullOrEmpty(result.errorMessage))
            {
                // truncate error messages but make sure severe errors are fully included
                if (errorStringBuilder.Length < MAX_ERROR_STRING_LENGTH || result.severeErrorOccured)
                {
                    // separate error messages with empty line
                    if (errorStringBuilder.Length != 0)
                    {
                        errorStringBuilder.Append('\n');
                    }

                    errorStringBuilder.Append(result.errorMessage);
                }
                else if (!truncated)
                {
                    // mark truncation for the first time
                    errorStringBuilder.Append("\n=====TRUNCATED=====\n");
                    truncated = true;
                }
            }

            if (!result.severeErrorOccured) continue;
            message.ErrorMessage = errorStringBuilder.ToString();

            if (result.invalidLegacyServerKey)
            {
                throw new MessageSendServerKeyException(result.errorMessage);
            }
            
            throw new MessageSendException(result.errorMessage);
        }

        if (invalidRegistrationsQueue.Count > 0) {
            await _repository.RemoveGCMDeviceRegistrationsAsync(message.PublisherId,
                message.AppOwnerUsername, message.AppId, invalidRegistrationsQueue);
        }
        
        message.ErrorMessage = errorStringBuilder.ToString();
    }
    }
}