using System.Collections.Concurrent;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using Model.Scheduler;

namespace ApplicationCore.Scheduler
{
    public class SchedulerService : ICloudSchedulerService
    {
        private const int MaxMessagesInQueue = 1000;
        private readonly IContextRepository _context;
        public ConcurrentQueue<SchedulerMessage> Queue { get; private set; }

        public SchedulerService(IContextRepository context)
        {
            _context = context;
            Queue = new ConcurrentQueue<SchedulerMessage>();
        }

        public Task DoMaintenance()
        {
            throw new System.NotImplementedException();
        }

        public Task Start()
        {
            throw new System.NotImplementedException();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }

        public bool Enqueue(SchedulerMessage message)
        {
            if (Queue.Count >= MaxMessagesInQueue) return false;
            Queue.Enqueue(message);
            return true;
        }

        public string GetStats()
        {
            throw new System.NotImplementedException();
        }
    }
}