using System.Threading.Tasks;
using Model;
using Model.Scheduler;

namespace ApplicationCore.Interfaces
{
    public interface ICloudSchedulerService
    {
        Task DoMaintenance();
        Task Start();
        void Stop();
        bool Enqueue(SchedulerMessage message);
        string GetStats();
    }
}