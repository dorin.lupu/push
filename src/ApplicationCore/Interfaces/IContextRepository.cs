using Model.Scheduler;

namespace ApplicationCore.Interfaces
{
    public interface IContextRepository {
        bool SaveSchedulerMessage(SchedulerMessage message);
    }
}