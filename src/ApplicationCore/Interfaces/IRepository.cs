using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Model;
using Model.PWA;

namespace ApplicationCore.Interfaces
{
    /// <summary>
    /// Summary description for IRepository
    /// </summary>
    public interface IRepository
    {
        #region APN

        Task<Dictionary<string, int>> APNSRegister(ConcurrentQueue<DeviceRegistration> registrationsQueue);
        
        Task<List<string>> GetAPNSRegistrations(Message message);

        Task<int> RemoveApnDeviceRegistrationsAsync(string publisherId, string userName, string appId,
            IEnumerable<string> deviceTokens);

        Task<List<string>> GetAPNSTopicSubscriptions(Topic topic);
        Task<Dictionary<string, object>> SetAPNSTopicSubscriptions(Topic topic);
        
        #endregion

        #region GCM
        Task<Dictionary<string, int>> GCMRegister(ConcurrentQueue<DeviceRegistration> registrationsQueue);
        
        Task<List<string>> GetGCMRegistrations(Message message);

        Task<int> RemoveGCMDeviceRegistrationsAsync(string publisherId, string userName, string appId,
            IEnumerable<string> deviceTokens);

        Task<List<string>> GetGCMTopicSubscriptions(Topic topic);
        Task<Dictionary<string, object>> SetGCMTopicSubscriptions(Topic topic);

        public Dictionary<string, int> DeleteInvalidRegistrations(ConcurrentQueue<string> invalidRegistrationsQueue,
            int regDeleteBatch, string type);
        
        public Task<bool> UpdateRegistrationId(string oldRegistrationId, string newRegistrationId);
        #endregion
    

        #region Pwa

        Dictionary<string, int> PwaRegister(ConcurrentQueue<PwaDeviceRegistration> registrationsQueue);

        List<PwaDeviceRegistration> GetPwaRegistrations(Message message);

        Dictionary<string, object> InsertPwaTopics(string topics, string publisherId, string username, string appId,
            string type,
            string deviceId);

        #endregion
    }
}