using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model;

namespace Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("/")]
    public class LegacyController : ControllerBase
    {
        private static string RESPONSE_OK = "OK";
        private readonly ICloudMessageManager _cloudMessageManager;
        private readonly ILogger<LegacyController> _logger;
        private readonly IRepository _repository;


        public LegacyController(ICloudMessageManager cloudMessageManager, ILogger<LegacyController> logger,
            IRepository repository)
        {
            _cloudMessageManager = cloudMessageManager;
            _logger = logger;
            _repository = repository;
        }

        [AllowAnonymous]
        [HttpPost("gcm_register.ashx")]
        public ActionResult RegisterAndroidDevice([FromForm] LegacyDeviceRegistration legacyDeviceRegistration)
        {
            if (legacyDeviceRegistration == null)
            {
                return BadRequest();
            }

            var registration = DeviceRegistration.GetFromLegacyDeviceRegistration(legacyDeviceRegistration);
            registration.Platform = Message.PLATFORM_GCM;
            if (string.IsNullOrEmpty(registration.PublisherId))
            {
                registration.PublisherId = "";
            }

            if (!registration.IsValid(out var validationArgs))
            {
                var error = "Invalid request. At least one of this params is invalid: " + validationArgs;

                var response = new ApiError();
                response.SetError(HttpStatusCode.BadRequest, "invalidParameters", error);

                return Ok(response);
            }

            if ("BLACKLISTED".Equals(registration.RegistrationId))
            {
                var message = "Invalid request. Registration id is blacklisted";
                _logger.LogError(message);
                return BadRequest(message);
            }

            _cloudMessageManager.RegisterDevice(registration);
            return new ContentResult()
            {
                Content = RESPONSE_OK,
                ContentType = "text/plain; charset=utf-8"
            };
        }

        [AllowAnonymous]
        [HttpGet("gcm_get_topic_subscriptions.ashx")]
        public async Task<ActionResult> GetTopicSubscriptions([FromQuery] LegacyTopic legacy)
        {
            if (legacy == null)
            {
                return BadRequest();
            }

            var topic = Topic.GetFromLegacyTopic(legacy);

            if (topic == null)
            {
                return BadRequest();
            }

            topic.PublisherId ??= SCEnvironment.ScPublisherId;

            if (!topic.IsValid())
            {
                var apiError = new ApiError();
                apiError.SetError(HttpStatusCode.BadRequest, "invalidParameters",
                    "At least one of {publisherId}, {username}, {appId}, {deviceId}, {type} is invalid");
                return Ok(apiError);
            }

            var topics = await _repository.GetGCMTopicSubscriptions(topic);
            return new ContentResult
            {
                Content = RESPONSE_OK + "," + string.Join(",", topics),
                ContentType = "text/plain; charset=utf-8"
            };
        }

        [AllowAnonymous]
        [HttpPost("gcm_set_topic_subscriptions.ashx")]
        public async Task<ActionResult> PostTopicSubscriptions([FromQuery] LegacyTopic legacy)
        {
            if (legacy == null)
            {
                return BadRequest();
            }
            
            var topic = Topic.GetFromLegacyTopic(legacy);
            topic.Topics =  string.Join(",", Request.Form.Keys);;

            topic.PublisherId ??= SCEnvironment.ScPublisherId;

            if (!topic.IsValid())
            {
                var apiError = new ApiError();
                apiError.SetError(HttpStatusCode.BadRequest, "invalidParameters",
                    "At least one of {publisherId}, {username}, {appId}, {deviceId}, {type} is invalid");
                return Ok(apiError);
            }
            
            if (!string.IsNullOrWhiteSpace(topic.Topics))
            {
                var inserted = 0;
                var sqlResult = await _repository.SetGCMTopicSubscriptions(topic);
                var topicIds = (sqlResult["topicIds"] as string[]) ?? new string[] { };
                inserted = (int)sqlResult["inserted"];

                if (inserted != topicIds.Length)
                {
                    return BadRequest("Not all topics inserted");
                }
            }
            
            return new ContentResult
            {
                Content = RESPONSE_OK,
                ContentType = "text/plain; charset=utf-8"
            };
        }
    }
}