    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using ApplicationCore;
    using ApplicationCore.APNS;
    using ApplicationCore.GCM;
    using ApplicationCore.Interfaces;
    using ApplicationCore.Scheduler;
    using Infrastructure;
    using Infrastructure.psql;
    using Infrastructure.Services;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.IdentityModel.Tokens;
    using Microsoft.OpenApi.Models;
    using Model;
    using Model.Utils;

    namespace Api
    {
        public class Startup
        {
            public Startup(IConfiguration configuration)
            {
                Configuration = configuration;
            }

            public IConfiguration Configuration { get; }

            // This method gets called by the runtime. Use this method to add services to the container.
            public void ConfigureServices(IServiceCollection services)
            {
                services.AddControllers();
                services.AddSingleton<IRepository>(provider =>
                    new DatabaseRepository(Configuration.GetConnectionString("CloudResConnection"),
                        provider.GetService<ILogger<DatabaseRepository>>()));

                services.AddSingleton(new SCEnvironment(Configuration.GetValue("CertPath", "certs"),
                    Configuration.GetValue("CertPass", "")));
                services.AddSingleton<IApnHelper, ApnHelper>();
                services.AddSingleton<ICloudMessageService, ApnService>(provider =>
                    {
                        var repository = provider.GetService<IRepository>();
                        var logger = provider.GetService<ILogger<ApnService>>();
                        var env = new SCEnvironment(Configuration.GetValue("CertPath", "certs"),
                            Configuration.GetValue("CertPass", ""));
                        var apnHelper = provider.GetService<IApnHelper>();
                        var workers = new List<IMessageWorker>();
                        var loggerWorker = provider.GetService<ILogger<IMessageWorker>>();

                        for (var i = 1; i <= SCEnvironment.MaxWorkers; i++)
                        {
                            workers.Add(new ApnMessageWorker(repository, env, apnHelper, loggerWorker,
                                CryptoUtil.GetRandomAlphanumericString(8)));
                        }

                        var maintenanceLogger = provider.GetService<ILogger<IMaintenanceWorker>>();
                        var maintenanceWorker = new MaintenanceWorker(CryptoUtil.GetRandomAlphanumericString(8),
                            maintenanceLogger, SCEnvironment.MaintenancePeriod);
                        return new ApnService(repository, logger, workers, maintenanceWorker);
                    }
                );
                
                services.AddSingleton<ICloudMessageService, GcmService>(provider =>
                    {
                        var repository = provider.GetService<IRepository>();
                        var logger = provider.GetService<ILogger<GcmService>>();
                        var env = new SCEnvironment(Configuration.GetValue("CertPath", "certs"),
                            Configuration.GetValue("CertPass", ""));
                         
                        var workers = new List<IMessageWorker>();
                        var loggerWorker = provider.GetService<ILogger<GcmMessageWorker>>();

                        for (var i = 1; i <= SCEnvironment.MaxWorkers; i++)
                        {
                            workers.Add(new GcmMessageWorker(repository, env,loggerWorker, CryptoUtil.GetRandomAlphanumericString(8)));
                        }

                        var maintenanceLogger = provider.GetService<ILogger<IMaintenanceWorker>>();
                        var maintenanceWorker = new MaintenanceWorker(CryptoUtil.GetRandomAlphanumericString(8),
                            maintenanceLogger, SCEnvironment.MaintenancePeriod);
                        return new GcmService(repository, logger, workers, maintenanceWorker);
                    }
                );
                services.AddScoped<ITokenService, TokenService>();

                services.AddSingleton<ICloudMessageManager, CloudMessageManager>();
                services.AddDistributedRedisCache(options =>
                    {
                        if (!Configuration.GetValue<bool>("Redis:IsRedisEnabled", false))
                        {
                            return;
                        }

                        options.Configuration = Configuration.GetValue("Redis:Configuration", "127.0.0.1:6379");
                        options.InstanceName = Configuration.GetValue("Redis:master", "master");
                    }
                );
                services.AddControllers().AddNewtonsoftJson();

                services.AddSwaggerGen(options =>
                {
                    options.SwaggerDoc("v1", new OpenApiInfo { Title = "Api", Version = "v1" });

                    // using System.Reflection;
                    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
                    
                    
                    var securityScheme = new OpenApiSecurityScheme
                    {
                        Name = "JWT Authentication",
                        Description = "Enter JWT Bearer token **_only_**",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.Http,
                        Scheme = "bearer", // must be lower case
                        BearerFormat = "JWT",
                        Reference = new OpenApiReference
                        {
                            Id = JwtBearerDefaults.AuthenticationScheme,
                            Type = ReferenceType.SecurityScheme
                        }
                    }; 
                    
                    options.AddSecurityDefinition(securityScheme.Reference.Id, securityScheme);
                    options.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        {securityScheme, new string[] { }}
                    });
                       // add Basic Authentication
                    var basicSecurityScheme = new OpenApiSecurityScheme
                    {
                        Type = SecuritySchemeType.Http,
                        Scheme = "basic",
                        Reference = new OpenApiReference { Id = "BasicAuth", Type = ReferenceType.SecurityScheme }
                    };
                    options.AddSecurityDefinition(basicSecurityScheme.Reference.Id, basicSecurityScheme);
                    options.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        {basicSecurityScheme, new string[] { }}
                    });
                });


                // configure jwt authentication 
                var key = Encoding.ASCII.GetBytes(Configuration["Secret"]);

                services.AddAuthentication(x =>
                    {
                        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    })
                    .AddJwtBearer(x =>
                    {
                        x.RequireHttpsMetadata = false;
                        x.SaveToken = true;
                        x.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(key),
                            ValidateIssuer = false,
                            ValidateAudience = false
                        };
                    });

                services.AddDbContext<NpgsqlContext>(options =>
                    options.UseNpgsql(Configuration.GetConnectionString("NewCloudResConnection"))
                        .UseSnakeCaseNamingConvention());
                
                services.AddScoped<IContextRepository>(provider => {
                    return  provider.GetService<NpgsqlContext>();
                });
          //      services.AddSingleton<ICloudSchedulerService, SchedulerService>();
            }
            
            public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
            {
                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                    app.UseSwagger();
                    app.UseSwaggerUI();
                }

                // app.UseHttpsRedirection();

                app.UseRouting();

                app.UseAuthentication();
                app.UseAuthorization();
                
                app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            }
        }
    }